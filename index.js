const server = require('./bin/www');
const puppeteer = require('puppeteer');

// require('./bin/www');
async function run() {

  const browser = await puppeteer.launch({headless: false});
  const page = await browser.newPage();
  page.on('console', msg => {
    console.log(msg.text());
  });
  await page.goto(`http://localhost:${process.env.PORT || 3000}`, 'domcontentloaded');

  const route = require('./src/routes/calendar');
  //await route.fillOpenAir();
}

server.on('listening', run);
