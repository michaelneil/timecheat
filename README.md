# Timecheat

Because I don't like entering time into OpenAir

 - Authenticate with Microsoft to gain access to the calendar
 - Determine the week you want to use (defaults to last week)
  - Currently not configurable
 - Opens and logs into OpenAir
 - Opens the new timesheet window and waits for you to create it
  - You can also open an existing timesheet
 - Automatically populates your timesheet once it's opened

## Setup

 - Copy .env.example to .env and fill it out
  - You'll need to get the APP_PASSWORD from Michael Neil
  - Alternatively [create your own Microsoft App](https://apps.dev.microsoft.com/) and [follow these instructions](https://docs.microsoft.com/en-us/outlook/rest/node-tutorial#register-the-app)
 - Copy mappings.example.json to mappings.json
  - Put it in the root of the project (next to .env)
  - You can use this to map recurring or often used events to projects and tasks

This works best when you enter events in your calendar for everything you do. I've stopped using openair to track time
and I instead mark what I'm doing in outlook. Outlook has a mobile app (openair does not) and the outlook ui is much better for entering time/tasks into. Try to name your outlook tasks closely to the project names in OpenAir. This ensures the highest match rate and will help enter more time automatically.

### Mappings (JSON)

A mapping helps you translate a calendar event into an OpenAir event. The app will try to automatically map your events
using levenshtien but it is not always accurate - especially for Helios Admin tasks. Mappings are helpful for recurring events or
events you schedule often. For example, if you are assigned to AARP as a project you might add a mapping for AARP to the list so that it knows calendar events named AARP go to a specific project. You may also use a mapping to set scope automatically.

An example mapping:

```
{
  "weekly team update": "helios : admin",
  "brainstorm": "helios : admin",
  "tuesday catered lunch": null,
  "weekly labs team meeting !": {
    "project": "helios : admin",
    "task": "internal meeting"
  },
  "pricing and feasibility": {
    "project": "helios : admin",
    "task": "internal meeting"
  },
  "dentist": null,
  "doctor": null
}
```
Mappings are key:value pairs where the key is the name of the event in your calendar and the value is the name of the project in OpenAir. The value doesn't have to be 100% the same. But, the more accurate it is the more likely it will track to the correct project. The value can also be an object where value.project is the project name, and value.task is the task. 


## Using

Run the app with `npm start` and go to `http://localhost:3000`. From here you can authenticate with Microsft OAuth
for access to outlook.

View the calendar by clicking on the Calendar menu item after you have successfully logged in.

Click the timesheet button to start the open air process. OpenAir should be logged into automatically (if your env is filled out). Create a new timesheet or open an existing one.

> All events go into a timesheet. Only edit an existing one if you want to add all events from last week to that sheet

You can choose to create a new timesheet here or browse to an existing timesheet.

Once the timesheet is open the app attempts to fill it out as much as possible. **This happens immediately**. A new row
is created for each event on each day.

### Roadmap

Calendar names are matched to job names in the csv using dice coefficient. Display the mapping in UI instead of in a file on disk.

The only calendar events outlook grabs for now is last week. Add a week selector to the calendar page. The math for selecting dates is simple and stubbed in with moment. We just need to pass the date to the server.