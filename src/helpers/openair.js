const puppeteer = require('puppeteer');
const url = require('url');
const levenshtein = require('fast-levenshtein');
const moment = require('moment');
const stringSimilarity = require('string-similarity');

const DEFAULT_TASK = "1. In Scope";

/** 
 * Initiate a new browser session and return page handle
 */
async function start(){
  const width = 1440;
  const height = 800;
  const browser = await puppeteer.launch({
    headless: false,
    // devtools: true,
    args: [
      `--window-size=${width},${height}`,
    ],
  });
  const page = await browser.newPage();
  page.setViewport({ width, height });
  page.on('console', msg => {
    console.log(msg.text());
  });
  return page;
}
/**
 * Login to OpenAir
 * @param {} page 
 */
async function login(page) {
  await page.goto('https://www.openair.com/index.pl', 'domcontentloaded');
  // wait for the form to load
  const form = await page.waitFor('#input_company');
  // fill the form and submit
  await page.evaluate((env) => {
    return new Promise((resolve) => {
      const input_company = document.querySelector('#input_company');
      input_company.value = 'freemanxp';
      const input_user = document.querySelector('#input_user');
      input_user.value = env.OA_USER;
      const input_password = document.querySelector('#input_password');
      input_password.value =  env.OA_PASS;
      const oa_comp_login_submit = document.querySelector('#oa_comp_login_submit');
      oa_comp_login_submit.click();
      resolve();
    });
  }, process.env);
}
/**
 * Click the buttons to make a new timesheet
 * It does not actually make the timesheet - just opens the page
 * @param {*} page 
 */
async function newTimesheet(page) {

  await delayFunc(async () => {
    const button = await page.waitFor('#oa3_toolbox_create_new');
    await button.click();
  }, 3500);

  await delayFunc(async () => {
    const container = await page.waitFor('#oa3_global_create_new');
    const anchors = await container.$$('a');
    const newAnchor = anchors[5];
    await newAnchor.click();
  }, 1500);
}
/**
 * Get a list of jobs in open air to fill timesheet with
 */
async function getJobs(page) {
  // timesheet

  const searchInput = await openProjectSearch(page);
  await delayFunc(async () => Promise.resolve(), 1000);
  // const container = await page.waitFor('#timesheet_grid');
  const parseResponse = new Promise((resolve) => {
    const onResponse =  async (res) => {
      if (res.url().indexOf('find.pl') === -1) return;
      const body = await res.text();
      let projects = body.split('\n');
      projects = projects.filter((project) => {
        // make sure the number 37 exists in the string. All Helios jobs end with 37
        return project.indexOf('37)') !== -1 || project.indexOf('Helios : Admin') !== -1;
      }).map((project) => {
        const split = project.split('^');
        return {text: split[0], value: split[1]};
      });
      const close = await page.$('.popup_close');
      await close.click();
      page.removeListener('response', onResponse);
      resolve(projects);
    }
    page.addListener('response', onResponse);
  });
  // const searchIcon = await page.$('#toggle_panel_imagesearch_cp__c1_r1');
  // await searchIcon.click();
  // const searchInput = await page.$('#search_string__c1_r1');
  await searchInput.type(' ');
  searchInput.press('Enter');

  return parseResponse;
}
/**
 * This function waits indefinitely until the #timesheet_grid element is available
 * It will then open the search box and return a handle on the search input
 * @param {*} page 
 * @returns {ElementHandle} searchInput
 */
async function openProjectSearch(page) {
  try {
    await page.waitFor('#timesheet_grid');
  } catch(e) {
    return await openProjectSearch(page);
  }
  const searchIcons = await page.$$('.timesheetAdvancedSearchLink');
  const searchIcon = searchIcons[searchIcons.length - 1];
  await searchIcon.click();
  await page.waitFor('.ac_input');
  const searchInputs = await page.$$('.ac_input');
  const searchInput = searchInputs[searchInputs.length - 1];
  return searchInput;
}
/**
 * Map calendar events to jobs in OpenAir
 * @param {*} week 
 * @param {*} jobs 
 */
function mapEventsToJobs(week, jobs) {

  const mappings = require('../../mappings.json');

  week = week.map((event) => {
    event.subject = event.subject.toLowerCase();
    return event;
  });

  const matches = [];
  week.forEach((event) => {
    event.subject = event.subject.toLowerCase();
    event.map = {};
    event.distance = 0;
    event.task = DEFAULT_TASK;
    // change the name of the event (helps to map to project later)
    if (mappings.hasOwnProperty(event.subject)) {
      if (mappings[event.subject] === null) return;
      event.originalSubject = event.subject;
      event.map = mappings[event.subject];
      
      if (typeof mappings[event.subject] === 'object') {
        event.subject = mappings[event.subject].project;
        if (event.map.task) {
          event.task = event.map.task;
        }
      } else {
        event.subject = mappings[event.subject];
      }
    }
    // push then assign. We can't use index of current scope because we may omit some of these
    matches.push(event);
    const match = matches[matches.length - 1];
    
    jobs.forEach((job) => {
      // TODO: do not lowercase here
      var similarity = stringSimilarity.compareTwoStrings(event.subject.toLowerCase(), job.text.toLowerCase()); 
      // const distance = levenshtein.get(event.subject, job.text.toLowerCase());
      if (similarity > match.distance) {
        match.distance = similarity;
        match.job = job;
        // match.job.task = match.map.task;
      }
    });
  });

  return matches;
}
/**
 * Given an array of options and a task value returns the best option match
 * @param {Array} options 
 * @param {string} task 
 */
function findMatchingTask(options, task) {
  if (task.toLowerCase() === DEFAULT_TASK.toLowerCase()) return options[1];
  let bestMatch = options[1];
  let closest = 0;
  options.forEach((option) => {
    let similarity = stringSimilarity.compareTwoStrings(task.toLowerCase(), option.text.toLowerCase()); 
    if (similarity > closest) {
      closest = similarity;
      bestMatch = option;
    }
  });
  return bestMatch;
}
/**
 * Add a row of time to the timesheet (timesheet must be open in page)
 * @param {} page 
 * @param {*} index 
 * @param {*} value 
 */
async function enterRow(page, row, index = 0) {

  // open search
  const searchInput = await openProjectSearch(page);
  // scroll to bottom left of page - otherwise we get errors with search box display
  await page.evaluate(() => {
    window.scrollTo(0, document.body.scrollHeight);
  });
  // every now and then this fires too early, set a delay here
  await delayFunc(async () => Promise.resolve(), 500);
  // open air requires slow typing into forms. Type at least the first 15 characters for a good match
  let split = 15;
  if (row.job.text.length < split) {
    split = row.job.text.length;
  }
  // fill in the search to select a project
  const searchSlow = row.job.text.slice(0, split);
  const searchFast = row.job.text.slice(split);
  await searchInput.type(searchSlow, {delay: 300});
  await searchInput.type(searchFast);
  // click the project results
  const ac_results = await page.$$('.ac_results');
  const ac_result = ac_results[index];
  const item = await ac_result.$('li');
  await item.click();

  const rows = await page.$$('table.timesheet tbody tr');
  const tds = await  rows[index].$$('td');
  const tdOffset = 2; // the first td with a date is td[3]
  // const options = await tds[2].$$('option');
  // Select a task
  const tasks = await page.evaluate((index) => {
    const trs = document.querySelectorAll('table.timesheet tbody tr');
    const tds = trs[index].querySelectorAll('td');
    const options = tds[2].querySelectorAll('option');
    const reducedOptions = Array.prototype.slice.apply(options).map((option) => {
      return {
        text: option.text,
        value: option.value
      }
    });
    return reducedOptions;
  }, index);
  const task = findMatchingTask(tasks, row.task);
  await page.select(`table.timesheet tbody tr:nth-child(${index+1}) td:nth-child(3) select`, task.value);

  async function fillDay(row) {
    const start = moment(`${row.start.dateTime.substr(0, row.start.dateTime.length - 4)}Z`);
    const end = moment(`${row.end.dateTime.substr(0, row.end.dateTime.length - 4)}Z`);
    const day = start.isoWeekday(); // 1-7 Mon - Sun
    const hours = end.diff(start, 'minutes');
    const input = await tds[tdOffset + day].$('input');
    await input.type('' + (hours / 60));
  }
  await fillDay(row);
}

/**
 * Delays the execution of a function
 * @param {*} func 
 * @param {*} time 
 */
function delayFunc(func, time) {
  return new Promise((resolve) => {
    setTimeout(async () => {
      await func();
      return resolve();
    }, time);
  });
}


module.exports = {
  login,
  start,
  newTimesheet,
  enterRow,
  getJobs,
  mapEventsToJobs,
};