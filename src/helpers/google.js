const fs = require('fs');
const path = require('path');

async function readSheet() {
  return new Promise((resolve) => {
    fs.readFile(path.resolve(__dirname, '../../', '0. Job Number Master List - 2018 - IOs.csv'), 'utf8', (err, file) => {

      console.log('the file', file);
      resolve(file);
    });
  });
}

module.exports = {
  readSheet,
}