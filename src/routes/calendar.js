const express = require('express');
const router = express.Router();
const authHelper = require('../helpers/auth');
const graph = require('@microsoft/microsoft-graph-client');
const moment = require('moment');
const openair = require('../helpers/openair');
const google = require('../helpers/google');
const fs = require('fs');
const path = require('path');

async function getCalendarWeek(accessToken, start) {
  // Initialize Graph client
  const client = graph.Client.init({
    authProvider: (done) => {
      done(null, accessToken);
    }
  });
  // Set end of the calendar view to 7 days from start
  const end = moment(start).add(7, 'days').endOf('day');
  const result = await client
  .api(`/me/calendarView?startDateTime=${start.toISOString()}&endDateTime=${end.toISOString()}`)
  .top(100)
  .select('subject,start,end')
  .orderby('start/dateTime DESC')
  .get();

  return result.value;
}
/**
 * Gets (n) weeks in the past. Default to last week (-1 week)
 * @param {*} weeks 
 */
function calculatePastWeeks(weeks = 1) {
  return moment().startOf('week').subtract(weeks * 7, 'days')
};

/* GET /calendar */
router.get('/', async function(req, res, next) {
  let parms = { title: 'Calendar', active: { calendar: true } };

  const accessToken = await authHelper.getAccessToken(req.cookies, res);
  const userName = req.cookies.graph_user_name;

  if (accessToken && userName) {
    parms.user = userName;
    try {
      parms.events = await getCalendarWeek(accessToken, calculatePastWeeks());
      res.render('calendar', parms);
    } catch (err) {
      parms.message = 'Error retrieving events';
      parms.error = { status: `${err.code}: ${err.message}` };
      parms.debug = JSON.stringify(err.body, null, 2);
      res.render('error', parms);
    }

  } else {
    // Redirect to home
    res.redirect('/');
  }
});
/**
 * Get the jobs and calendar events for a week in the past.
 * @param {Page} page 
 * @param {String} accessToken 
 * @param {Integer} week 
 */
async function getData(page, accessToken, week) {
  let events;
  let jobs;
  if (process.env.NODE_ENV === 'DEBUG') {
    events = require('../../debug/test-events.json');
    jobs = require('../../debug/test-projects.json');
  } else {
    jobs = await openair.getJobs(page);
    events = await getCalendarWeek(accessToken,  calculatePastWeeks(week));
  }
  return {events, jobs};
}
/**
 * Write the event/job matches to disk
 * @param {*} matches 
 */
function writeMatchesToDisk(matches) {
  return new Promise((resolve) => {
    fs.writeFile(path.resolve(__dirname, '../../', 'eventdump.json'), JSON.stringify(matches, null, 2), (err, res) => {
      console.log('wrote eventdump.json');
      console.log('Check this file to see how your events were mapped to jobs');
      console.log(err, res);
      resolve()
    });
  });
}
/**
 * Automate filling open air timesheet
 */
async function fillOpenAir(accessToken, date) {
  let page = await openair.start();
  await openair.login(page);
  // automatically make a new timesheet
  // await openair.newTimesheet(page);
  let {events, jobs} = await getData(page, accessToken,  date);
  const matches = openair.mapEventsToJobs(events, jobs);
  await writeMatchesToDisk(matches);
  async function iterate (matches, index = 0) {
    if (!matches[index]) return;
    await openair.enterRow(page, matches[index], index);
    return await iterate(matches, index + 1);
  }
  await iterate(matches);
}

router.post('/openair', async (req, res, next) => {
  const accessToken = await authHelper.getAccessToken(req.cookies, res);
  // TODO: Set weeks in the past here. Example 2 (2 weeks ago)
  fillOpenAir(accessToken);
  res.redirect('/calendar');
});

module.exports = router;
module.exports.getCalendarWeek = getCalendarWeek;
module.exports.fillOpenAir = fillOpenAir;