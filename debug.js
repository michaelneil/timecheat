/** 
 * Node debugger should use this as the entrypoint.
 * Add a launch configuration to your workspace or user settings
 * in order to run this file automatically on debug. 
 * 
 * Example using nvs and vscode
   "launch": {
      "version": "0.2.0",
      "configurations": [
        {
          "type": "node",
          "request": "launch",
          "name": "Launch Program",
          "program": "${workspaceFolder}\\debug.js",
          "args": [ ],
          "runtimeArgs": [ "run", "9.8.0" ],
          "windows": { "runtimeExecutable": "nvs.cmd" },
          "osx": { "runtimeExecutable": "nvs" },
          "linux": { "runtimeExecutable": "nvs" }
        }
      ]
    }
 */
// process.env.NODE_ENV = 'DEBUG';
require('./bin/www');

// async function test() {
//   require('dotenv').config();
//   const route = require('./src/routes/calendar');
//   await route.fillOpenAir();
// }
// test();
